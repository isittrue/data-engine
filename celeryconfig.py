from celery.schedules import crontab

# Broker settings

## Redis
BROKER_URL = 'redis://localhost:6696'

# List of modules to import when celery starts.
# CELERY_IMPORTS = ('tasks.spiders', 'tasks.scheduler')

ADMINS = [
    ('Emiliano M. Rudenick', 'notifications@merfrei.com'),
]

CELERY_SEND_TASK_ERROR_EMAILS = True

CELERYBEAT_SCHEDULE = {
    'check_scheduled_tasks_every_hour': {
        'task': 'tasks.scheduler.run_scheduled_tasks',
        'schedule': crontab(minute=0, hour='*'),
    },
}
