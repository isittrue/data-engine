# IsItTrue.io - Data Engine

Web scraping solution and API for making searches and collecting fact checks data from the web.

[List of targets](./doc/WEBSITES.md)

[API Documentation](./doc/API.md)
