"""
API: init routes
"""

from api.routes.search import init_routes as init_search_routes

def init_routes(app):
    '''Init some routes for the App'''
    init_search_routes(app)
