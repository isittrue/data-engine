"""
Search routes
"""

from api.handlers.search import search_handler


def init_routes(app):
    app.router.add_route('GET', r'/search', search_handler)
