"""
API handlers
"""

import datetime
import json

from bson.objectid import ObjectId

from aiohttp import web

from pydantic import ValidationError


def parse_datetime(content):
    '''Parse a datetime parameter
    I recommend to set the str format in a config file'''
    if isinstance(content, datetime.datetime):
        return content.strftime('%Y-%m-%d %H:%M:%S')
    if isinstance(content, datetime.date):
        return content.strftime('%Y-%m-%d')
    if isinstance(content, ObjectId):
        return str(content)
    return content


def custom_dumps(content):
    '''Custom dumps function for parsing datetime parameters'''
    return json.dumps(content, default=parse_datetime)


def get_404_response():
    '''Default 404 response'''
    return web.json_response({'message': 'Not found',
                              'data': {},
                              'status': 'unknown'}, status=404)


def get_422_response(details):
    '''Default validation error response'''
    return web.json_response({'message': 'Validation Errors',
                              'data': {'details': details},
                              'status': 'error'}, status=422)


def validate_params(params, schema):
    '''Given a pydantic model it will validate the body params'''
    try:
        schema_inst = schema(**params)
    except ValidationError as errors:
        return errors, params
    ret_params = {sch_k: sch_v for sch_k, sch_v in schema_inst.dict().items()
                  if sch_v is not None}
    return None, ret_params
