"""
Spider search handler

- Locate the spider class

- Initialize a new spider instance

- Run a new crawl to collect the results

- Return the results as a JSON
"""

import os
import uuid
import billiard

from aiohttp import web

from pydantic import BaseModel
from pydantic import validator

from typing import Optional

from api.handlers import custom_dumps
from api.handlers import validate_params
from api.handlers import get_404_response
from api.handlers import get_422_response

from scrap.crawler import CrawlerError
from scrap.crawler import run_crawler


BASE_DIR = '/tmp/isittrue'


MAX_PAGES = 5


def generate_data_dir(site_key, crawl_key):
    '''Given a key/site and a unique crawl ID it will create a path string
    to store the crawl data'''
    crawl_dir = os.path.join(f'{site_key}', crawl_key)
    return os.path.join(BASE_DIR, crawl_dir)


class SearchAPI(BaseModel):
    site: str
    query: str
    pages: Optional[int] = None

    @validator('site')
    def site_shoud_not_be_empty_nor_too_long(cls, v):
        v = v.strip()
        if not v:
            raise ValueError('Site is empty')
        if len(v) > 100:
            raise ValueError('Site is not valid')
        return v

    @validator('query')
    def query_should_not_be_empty(cls,v ):
        v = v.strip()
        if not v:
            raise ValueError('Query is empty')
        return v

    @validator('pages')
    def pages_greater_than_one(cls, v):
        if v < 1:
            raise ValueError('Page must be >= 1')
        return v


async def search_handler(request):
    params = {
        'site': request.query.get('site'),
        'query': request.query.get('query'),
        'pages': request.query.get('pages', MAX_PAGES),
    }
    errors, params = validate_params(params, SearchAPI)
    if errors is not None:
        return get_422_response(errors.json())

    manager = billiard.Manager()
    results = manager.list()
    crawl_id = str(uuid.uuid4())
    data_dir = generate_data_dir(params['site'], crawl_id)

    try:
        run_crawler(params['site'], data_dir, search_query=params['query'], results=results)
    except CrawlerError as crw_err:
        return web.json_response({'message': f'Errors found: {repr(crw_err)}',
                                  'data': {
                                      'uuid': crawl_id,
                                      'results': [],
                                  },
                                  'status': 'error'}, status=500)

    return web.json_response({'message': 'All OK',
                              'data': {
                                  'uuid': crawl_id,
                                  'results': list(results),
                              },
                              'status': 'success'}, status=200, dumps=custom_dumps)
