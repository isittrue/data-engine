"""
Crawler

Here are the function to start/run a crawler
"""

import os

from billiard import Process  # pylint: disable=no-name-in-module

from scrapy import Spider
from scrapy.crawler import CrawlerProcess

from scrap import settings as spider_base_settings
from scrap.index import SpiderIndex


class CrawlerError(Exception):
    '''Something went wrong'''


def load_settings():
    '''Read the Scrapy settings file and return it as a dict'''
    return {s: v for s, v in
            spider_base_settings.__dict__.items()
            if ((not s.startswith('_')) and s.isupper())}


def write_pid_file(pid: int, pidfile: str):
    '''Write the current PID to a file'''
    with open(pidfile, 'w') as f_pid:
        f_pid.write('{}'.format(pid))


def crawl(settings: dict, spider_class: Spider, *args, **kwargs):
    '''Start the crawl process'''
    crawler = CrawlerProcess(settings)
    crawler.crawl(spider_class, *args, **kwargs)
    crawler.start()
    crawler.stop()


def run_crawler(spider_key: str, data_dir: str = None,
                logfile: str = None, pidfile: str = None, **params):
    '''Start/Run the new crawl'''

    index = SpiderIndex()

    spider = index[spider_key]
    if spider is None:
        raise CrawlerError('"{}" does not exist in index'.format(spider_key))

    if data_dir is None:
        raise CrawlerError('A data directory is required and it can\'t be None')

    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    if logfile is None:
        logdir = os.path.join(data_dir, 'logs')
        if not os.path.exists(logdir):
            os.mkdir(logdir)
        logfile = os.path.join(logdir, 'spider.log')

    if pidfile is None:
        pidfile = os.path.join(data_dir, 'crawl.pid')

    crw_kwargs = {
        'data_dir': data_dir,
    }

    crw_kwargs.update(params)

    config = load_settings()
    config['LOG_FILE'] = logfile

    process = Process(target=crawl, args=[config, spider], kwargs=crw_kwargs)
    process.start()

    write_pid_file(process.pid, pidfile)

    process.join()
    process.terminate()
