"""
Run a spider
"""

import sys
import os
import logging
import argparse

from scrap.crawler import run_crawler


LOGFORMAT = '%(levelname)s: %(asctime)s %(filename)s: %(message)s'


parser = argparse.ArgumentParser(description="IsItTrue.io - Run a spider by command line")
parser.add_argument('--spider', type=str, dest='spider', required=True, help='The spider name to run')
parser.add_argument('--directory', type=str, dest='directory', required=True, help='The spider working directory')
parser.add_argument('--logfile', type=str, dest='logfile', required=False, help='A path to the LOG file, optional')
parser.add_argument('--pidfile', type=str, dest='pidfile', required=False, help='A path to the PID file, optional')
parser.add_argument('--params', type=str, dest='params', required=False,
                    help='Arguments to pass to the spider (--params "k1:v1,k2:v2,kn:vn")')


def parse_crawler_params(params_str: str):
    '''Convert the params string to a dict and return it'''
    params = {}
    if not params_str:
        return params
    for param_t in (params_str or '').split(','):
        param_k, param_v = param_t.split(':')
        params[param_k] = param_v
    return params


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format=LOGFORMAT, datefmt='%Y-%m-%d %H:%M:%S')

    parsed_args = parser.parse_args()

    params = {
        'spider_key': parsed_args.spider,
        'data_dir': parsed_args.directory,
    }

    if parsed_args.logfile:
        params['logfile'] = parsed_args.logfile
    if parsed_args.pidfile:
        params['pidfile'] = parsed_args.pidfile

    params.update(**parse_crawler_params(parsed_args.params))

    run_crawler(**params)
