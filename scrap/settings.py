"""
Scrapy Settings
"""

import os

from config import get_config

here = os.path.dirname(os.path.abspath(__file__))


def get_from_local_config(section, attribute, default=None):
    '''Get a local config value'''
    config = get_config()
    if section not in config:
        return default
    return config[section].get(attribute, default)


CONCURRENT_REQUEST = 64

RETRY_TIMES = 9
RETRY_HTTP_CODES = [500, 501, 502, 503, 504, 400, 401, 408, 403, 407, 456, 429]


USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36'


ITEM_PIPELINES = {
    'scrap.pipelines.mongo.ExportMongoPipeline': 301,
    'scrap.pipelines.results.ResultsPipeline': 302,
}


DOWNLOADER_MIDDLEWARES = {
    'scrap.downloadermiddlewares.cache.ArticleCacheDownloaderMiddleware': 400,
    'user_agent_scrapy.middlewares.UserAgentMiddleware': 401,
    'user_agent_scrapy.middlewares.UserAgentScrapyMiddleware': 402,
    'proxy_service_scrapy.middlewares.ProxyServiceMiddleware': 510,
    'scrap.downloadermiddlewares.errors.ErrorsDownloaderMiddleware': 543,
    'scrap.downloadermiddlewares.LogProxy': 570,
    'scrap.downloadermiddlewares.CustomHeadersMiddleware': 755,
    'scrap.httpcache.RedisCacheMiddleware': 900,
    'puppeteer_middleware.middleware.PuppeteerAPIMiddleware': 915,
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
}

SPIDER_MIDDLEWARES = {
    'scrap.spidermiddlewares.errors.ErrorsSpiderMiddleware': 910,
}


EXTENSIONS = {
    'scrap.extensions.crawler.CrawlerExtension': 600,
    'scrap.extensions.errors.ErrorsExtension': 650,
    'scrapy.extensions.statsmailer.StatsMailer': 700,
}

TELNETCONSOLE_ENABLED = False


# User Agent API Config
USER_AGENT_SERVICE_API_URL = 'http://127.0.0.1:7575'
USER_AGENT_SERVICE_API_KEY = 'aeghixie0aebadoh9Aip'

# Proxy Service Config
PROXY_SERVICE_API_URL = 'http://127.0.0.1:8585'
PROXY_SERVICE_API_KEY = 'eY3ori<o8Jei+ngai4Ae'

# Puppeteer API
PUPPETEER_API_URL = 'http://127.0.0.1:3580'

# Redis HTTP Cache
REDIS_CACHE_URI = 'redis://localhost:6698'
REDIS_CACHE_EXPIRATION_TIME = 60 * 60 * 24
REDIS_CACHE_DATA_PATH = os.path.join(here, 'cache')

# Export to MongoDB Pipeline
MONGO_URI = 'mongodb://localhost:28082'
MONGO_DATABASE = 'isittrue'

# Stats email recipients
STATSMAILER_RCPTS = get_from_local_config('notifications', 'stats', [])

# Email config
MAIL_HOST = 'localhost'
MAIL_FROM = get_from_local_config('notifications', 'sender')

# FirefoxHeadless config
FIREFOX_BINARY = get_from_local_config('firefox', 'binary')
FIREFOX_DRIVER = get_from_local_config('firefox', 'driver')
