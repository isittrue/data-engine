"""
Spider for crawling fact checks from snopes.com

URL: https://www.snopes.com/fact-check/
"""

import logging

from functools import wraps

from datetime import datetime

from w3lib.url import add_or_replace_parameter

from scrapy import Request

from scrap.lib.utils import clean_html

from scrap.spiders import BaseSpider


logger = logging.getLogger(__name__)


BLOCKED_CODES = [403, 500, 502, 503, 504]

MAX_RETRY = 9


def retry_blocked_request(*copy_meta_keys):
    '''A decorator to use in your parse method.
    It does not keep the meta keys. The meta keys you want to keep have to be passed
    in `copy_meta_keys`
    For example:

    @retry_blocked_request('productid', 'category')
    def parse_product(self, response):
        product_id = response.meta['productid']
        category = response.meta['category']
    '''
    def retry_decorator(p_m):
        @wraps(p_m)
        def pm_decorated(spider, response):
            if spider.is_blocked_response(response):
                logger.warning('Blocked response in %s', response.url)
                yield spider.retry_request(response.request, *copy_meta_keys)
                return
            for res in p_m(spider, response):
                yield res
        return pm_decorated
    return retry_decorator


class SnopesSpiderError(Exception):
    '''Something went wrong'''


class SnopesSpider(BaseSpider):
    name = 'snopes.com'
    allowed_domains = ['snopes.com']
    start_urls = ['https://www.snopes.com/fact-check/']

    download_timeout = 30

    handle_httpstatus_list = BLOCKED_CODES
    # We don't want to use automatic retries for response codes here, only for exceptions
    # In case you want to detect blocking catching exceptions too you can do it using `Request.errback`
    custom_settings = {
        'RETRY_HTTP_CODES': [],
    }

    ua_enabled = True

    ps_target = 'snopes'
    ps_len = '100'

    def init_search(self, response):
        '''Search by the self.search_query and collect the results
        Search URL example: https://www.snopes.com/?s=was+the+mail+in+votes+fraudulent
        '''
        base_url = 'https://www.snopes.com/'
        search_url = add_or_replace_parameter(base_url, 's', self.search_query)
        yield Request(search_url, meta={'pp_enabled': True, 'pp_waituntil': 'networkidle0'}, callback=self.parse_results)

    @retry_blocked_request()
    def parse(self, response):
        '''Article URLs and paging'''
        article_urls = response.css('main article > a::attr(href)').extract()
        next_page = response.css('ul.pagination a:contains("Next")::attr(href)').get()

        for url in article_urls:
            yield Request(response.urljoin(url), callback=self.parse_article)

        if next_page is not None:
            yield Request(response.urljoin(next_page))

    @retry_blocked_request('pp_enabled', 'pp_waituntil', 'page_no')
    def parse_results(self, response):
        '''Parse the search results'''
        self.logger.debug('Parsing results')
        article_urls = response.xpath(
            '//a[contains(@class, "search-entry") and contains(@class, "link") '
            'and contains(@href, "/fact-check/")]/@href').extract()

        self.logger.debug('%d results found', len(article_urls))

        for url in article_urls:
            article_url = response.urljoin(url)
            yield Request(article_url,
                          callback=self.parse_article,
                          meta={'cache__url': article_url})

        next_url = response.css('li.ais-pagination--item__next > a::attr(href)').get()
        if next_url is not None:
            next_page_no = int(response.meta.get('page_no', 1)) + 1
            if self.pages:
                if next_page_no > self.pages:
                    self.logger.debug('Escaping next page because page %d has been reached already', self.pages)
                    return
            self.logger.debug('Going page %d', next_page_no)
            yield Request(response.urljoin(next_url),
                          meta={'pp_enabled': True,
                                'pp_waituntil': 'networkidle0',
                                'page_no': next_page_no},
                          callback=self.parse_results)

    @retry_blocked_request('cache__url')
    def parse_article(self, response):
        '''Parse article content and fact check result'''
        title = response.css('header h1::text').get('').strip()
        subtitle = response.css('header h2::text').get('').strip()
        author_links = response.css('header .authors li a')
        authors = dict(zip([a_n.strip() for a_n in author_links.xpath('text()').extract()],
                            author_links.xpath('@href').extract()))
        date_published = response.css('.dates ::text').re_first(r'\d{2} \w+ \d{4}') or ''
        claim_txt = ' '.join([
            p_txt.strip() for p_txt in response.css('.claim-text ::text').extract()
            if p_txt.strip()])

        if not date_published and not claim_txt:
            moved_url = response.xpath(
                './/*[contains(text(), "This article has been moved here")]'
                '/parent::*//a/@href').get()
            if moved_url:
                yield Request(response.urljoin(moved_url),
                              meta=response.meta,
                              callback=self.parse_article)
            else:
                self.logger.warning('Unable to parse Date Published and Claim Text in %s', response.url)
            return

        if not title and not subtitle:
            raise SnopesSpiderError(f'Unable to parse article in {response.url}')

        article_id = response.xpath('//link[@rel="shortlink"]/@href').re_first(r'p=(\d+)')
        if article_id is None:
            raise SnopesSpiderError(f'No article ID in {response.url}')

        rating_txt = response.css('.media-body').xpath('./span[contains(@class, "rating-label")]/text()').get('').strip()
        whats_true = response.css('.media.whats-true p::text').get('').strip()
        whats_false = response.css('.media.whats-false p::text').get('').strip()
        whats_ubdetermined = response.css('.media.whats-undetermined p::text').get('').strip()

        try:
            article_date = datetime.strptime(date_published, '%d %B %Y')
        except ValueError:
            article_date = None

        category = '/'.join([
            cat.strip() for cat in
            response.css('.breadcrumbs .breadcrumb-item')
            .xpath('./a[contains(@href, "/category/")]/text()')
            .extract() if cat.strip()])

        main_text = clean_html(response.css('article .single-body').get(''))
        tags = [tag.strip() for tag in response.css('.tags a ::text').extract() if tag.strip()]

        citations = response.css('.citations .list-group-item')
        sources = []
        for citation_sel in citations:
            sources.append(
                ' '.join([c_txt.strip() for c_txt in
                          citation_sel.css('p ::text').extract()
                          if c_txt.strip]).strip())

        article_data = {
            'id': int(article_id),
            'url': response.url,
            'title': title,
            'subtitle': subtitle,
            'authors': authors,
            'dates': {
                'published': article_date,
            },
            'claim': claim_txt,
            'rating': {
                'rating': rating_txt,
                'whats_true': whats_true,
                'whats_false': whats_false,
                'whats_undetermined': whats_ubdetermined,
            },
            'category': category,
            'body': main_text,
            'tags': tags,
            'sources': sources,
        }

        yield article_data

    @staticmethod
    def is_blocked_response(response):
        return response.status in BLOCKED_CODES

    def retry_request(self, request, *copy_meta_keys):
        '''This method returns a new Request for retrying purposes.
        The original meta dict is not keeped. You can pass the meta values
        you want to keep in `copy_meta_keys`'''
        orig_url = request.meta.get('puppeteer_origin_url', request.url)
        retry_no = int(request.meta.get('retry_no', 0)) + 1
        if retry_no > MAX_RETRY:
            self.logger.warning('MAX NUMBER OF RETRIES REACHED FOR %s', orig_url)
            return None
        orig_meta = request.meta.copy()
        # The original URL is in `puppeteer_origin_url` in `request.meta`
        new_request = request.replace(url=orig_url)
        # Some other conflictive values are there, so we need to remove them
        # Scrapy do not allow me to replace the request in the response object on the middleware
        # Or I just couldn't make it work yet...
        new_request.meta.clear()
        for meta_key in copy_meta_keys:
            if meta_key in orig_meta:
                new_request.meta[meta_key] = orig_meta[meta_key]
                if meta_key == 'pp_enabled':
                    # This is disabled in the middleware
                    new_request.meta[meta_key] = True
        new_request.meta['recache'] = True
        new_request.meta['retry_no'] = retry_no
        new_request.dont_filter = True
        return new_request
