"""
Project spiders
"""

from scrapy import Spider
from scrapy import Request


DUMMY_URL = 'file:///etc/hosts'


class BaseSpider(Spider):
    '''Base spider with common features used by all the spiders'''

    def __init__(self, *args, **kwargs):
        # Search query when we used the search feature
        self.pages = int(kwargs.get('pages', 0))
        self.search_query = kwargs.get('search_query', '').strip()
        self.results = None
        # When search mode is enabled we can use a results instance
        if self.search_query:
            self.results = kwargs.get('results')

    def start_requests(self):
        if self.search_query:
            yield Request(DUMMY_URL, callback=self.init_search, dont_filter=True)
        else:
            for url in self.start_urls:
                yield Request(url)

    def init_search(self, response):
        '''Implement to support search mode'''
        raise NotImplementedError
