"""
Spider for crawling fact checks from factcheck.org

URL: https://www.factcheck.org/
"""

from datetime import datetime

from scrapy import Request

from scrap.lib.utils import clean_html

from scrap.spiders import BaseSpider


class FactcheckOrgSpiderError(Exception):
    '''Something went wrong'''


class FactcheckOrgSpider(BaseSpider):
    name = 'factcheck.org'
    allowed_domains = ['factcheck.org']
    start_urls = ['https://www.factcheck.org/']

    max_concurrent_requests = 1
    download_delay = 1

    ua_enabled = True

    ps_target = 'factcheck.org'
    ps_len = '100'

    questions_url = 'https://www.factcheck.org/askfactcheck/'
    posts_url = 'https://www.factcheck.org/the-factcheck-wire/'

    def init_search(self, response):
        return None

    def parse(self, response, **kwargs):
        '''There are two types of data extracted here:
        - Questions
        - Posts'''
        yield Request(self.questions_url,
                      meta={'parse_method': self.parse_question},
                      callback=self.parse_results)
        yield Request(self.posts_url,
                      meta={'parse_method': self.parse_post},
                      callback=self.parse_results)

    def parse_results(self, response):
        '''Common pages of article results'''
        article_urls = response.css('#main article.post h3 > a::attr(href)').extract()
        for url in article_urls:
            yield Request(response.urljoin(url),
                          callback=response.meta['parse_method'])

        next_page_url = response.css('li.page-item-next a::attr(href)').get()
        if next_page_url is not None:
            yield Request(response.urljoin(next_page_url),
                          meta=response.meta,
                          callback=self.parse_results)

    def parse_question(self, response):
        '''Parse a question article'''
        article = self.get_article_from_response(response)

        question = ''.join(response.css('.entry-content b:contains("Q:") > span ::text').extract()).strip()
        if not question:
            question = ''.join(response.css('.entry-content strong:contains("Q:")')
                               .xpath('./parent::*//text()').extract()).strip()
            question = question.split(':', 1)[-1].strip()
        answer = ''.join(response.css('.entry-content b:contains("A:") > span ::text').extract()).strip()
        if not answer:
            answer = ''.join(response.css('.entry-content strong:contains("A:")')
                             .xpath('./parent::*//text()').extract()).strip()
            answer = answer.split(':', 1)[-1].strip()

        full_question = []
        for elem_sel in response.css('h5:contains("FULL QUESTION")').xpath('./following-sibling::*'):
            if 'FULL ANSWER' in (txt.strip() for txt in elem_sel.xpath('.//text()').extract()):
                break
            full_question.append(' '.join([txt.strip() for txt in
                                           elem_sel.xpath('.//text()').extract()
                                           if txt.strip()]))
        full_question = ' '.join(full_question)

        full_answer = ''
        for elem_sel in response.css('h5:contains("FULL ANSWER")').xpath('./following-sibling::*'):
            if elem_sel.xpath('text()').get('').strip() == 'Sources':
                break
            full_answer += clean_html(elem_sel.extract())

        if not all([question, answer]):
            raise FactcheckOrgSpiderError(f'Missing data for question in {response.url}')

        article['question'] = question
        article['answer'] = answer
        article['full_question'] = full_question
        article['full_answer'] = full_answer

        yield article

    def parse_post(self, response):
        '''Parse a post article'''
        article = self.get_article_from_response(response)

        first_paragraph = response.css('.entry-content p ::text').get('').strip()
        body = ''
        for elem_sel in response.css('span#dpsp-post-content-markup').xpath('./following-sibling::*'):
            if elem_sel.xpath('text()').get('').strip() == 'Sources':
                break
            body += clean_html(elem_sel.extract())

        if not all([first_paragraph, body]):
            raise FactcheckOrgSpiderError(f'Missing data for post in {response.url}')

        article['first_paragraph'] = first_paragraph
        article['body'] = body

        yield article

    @staticmethod
    def get_datetime(dtstr, dtfrm):
        '''datetime.strptime managing exception and returning None when it fails'''
        try:
            return datetime.strptime(dtstr, dtfrm)
        except (TypeError, ValueError):
            return None

    @classmethod
    def get_article_from_response(cls, response):
        '''Get common data article'''
        article_id = response.css('article::attr(id)').re_first(r'post-(\d+)')
        categories = response.css('.breadcrumb-category > a::text').extract()
        article_title = response.css('h1.entry-title::text').get('').strip()
        author = response.css('*[rel="author"]::text').get('').strip()
        published = response.css('time.published::attr(datetime)').get()
        updated = response.css('.posted-on ::text').re_first(r'Updated on (.*)')

        if not all([article_id, categories, article_title, author, published]):
            raise FactcheckOrgSpiderError(f'Missing important data in {response.url}')

        sources = []
        for source_sel in response.css('h5:contains("Sources") ~ p'):
            sources.append(clean_html(source_sel.extract()))

        tags = response.css('footer .issue a::text').extract()
        tags.extend(response.css('footer .post_tag a::text').extract())

        location = response.css('footer .location a::text').extract()
        people = response.css('footer .person a::text').extract()

        return {
            'id': article_id,
            'url': response.url,
            'category': categories,
            'title': article_title,
            'authors': [author],
            'date': {
                'published': datetime.strptime(published.split('T')[0], '%Y-%m-%d'),  # We need to throw a exception if it fails here
                'updated': cls.get_datetime(updated, '%B %d, %Y') if updated else '',
            },
            'tags': tags,
            'sources': sources,
            'metadata': {
                'location': location,
                'people': people,
            }
        }
