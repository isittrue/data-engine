"""
Spider for crawling fact checks from snopes.com

URL: https://www.snopes.com/fact-check/
"""

from hashlib import md5
from datetime import datetime

from w3lib.html import get_base_url
import extruct

from scrapy import Request

from scrapy import signals

from scrap.lib.utils import clean_html

from scrap.spiders import BaseSpider


class AFPSpiderError(Exception):
    '''Something went wrong'''


class AFPSpider(BaseSpider):
    name = 'afp.com'
    allowed_domains = ['afp.com']
    start_urls = ['https://factcheck.afp.com/']

    ua_enabled = True

    ps_target = 'afp'
    ps_len = '100'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.region_done = False

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(cls, AFPSpider).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, signal=signals.spider_idle)
        return spider

    def spider_idle(self, spider):
        if not self.region_done:
            req = Request(self.start_urls[0], callback=self.parse_regions, dont_filter=True)
            spider.crawler.engine.crawl(req, self)

    def init_search(self, response):
        return None

    def parse(self, response):
        all_topics_tab_id = response.xpath(
            '//button[contains(text(), "TOPICS")]/@data-target').get()
        all_topic_links = response.css(
            f'{all_topics_tab_id} ul.nav-categories li.list-inline-item > a')
        for topic_link_sel in all_topic_links:
            topic_name = topic_link_sel.xpath('text()').get('').strip()
            if not topic_name:
                self.logger.error('Unable to get topic name')
                continue
            topic_url = topic_link_sel.xpath('@href').get()
            yield Request(response.urljoin(topic_url),
                          meta={'topic': topic_name},
                          callback=self.parse_list)

    def parse_regions(self, response):
        self.region_done = True
        all_regions_tab_id = response.xpath(
            '//button[contains(text(), "REGIONS")]/@data-target').get()
        all_region_links = response.css(
            f'{all_regions_tab_id} ul.nav-categories li.list-inline-item > a')
        for region_link_sel in all_region_links:
            region_name = region_link_sel.xpath('text()').get('').strip()
            if not region_name:
                self.logger.error('Unable to get region name')
                continue
            region_url = region_link_sel.xpath('@href').get()
            yield Request(response.urljoin(region_url),
                          meta={'region': region_name},
                          callback=self.parse_list)

    def parse_list(self, response):
        article_urls = response.css('.card > a::attr(href)').extract()
        for url in article_urls:
            yield Request(response.urljoin(url),
                          meta=response.meta,
                          callback=self.parse_article)

        next_page_url = response.css('a.page-link:contains(Next)::attr(href)').get()
        if next_page_url is not None:
            yield Request(response.urljoin(next_page_url),
                          meta=response.meta,
                          callback=self.parse_list)

    def parse_article(self, response):
        extruct_data = extruct.extract(response.text,
                               base_url=get_base_url(response.url),
                               syntaxes=['json-ld'])
        if not extruct_data['json-ld']:
            self.logger.warning('No data found for article: %s', response.url)
            return
        article = extruct_data['json-ld'][0]['@graph'][0]

        try:
            article_date = datetime.strptime(article['datePublished'], '%Y-%m-%d %H:%M')
        except (TypeError, ValueError):
            article_date = ''

        authors = [author.strip() for author in
                   response.css('.meta-author a::text').extract() if author.strip()]
        subtitle = response.css('.article-entry h3::text').get('').strip()
        main_text = clean_html(response.css('.article-entry').get('').strip())
        tags = [tag.strip() for tag in
                response.css('.tags a::text').extract() if tag.strip()]

        metadata = {
            'region': response.meta.get('region', ''),
        }
        if 'itemReviewed' in article:
            metadata['item_reviewed'] = {
                'url': article['itemReviewed'].get('url', ''),
                'date_published_str': article['itemReviewed'].get('datePublished', ''),
                'author': article['itemReviewed'].get('author', {}),
            }

        item = {
            'id': self.get_article_identifier(article),
            'topic': response.meta.get('topic', ''),
            'url': article['url'],
            'title': article['name'],
            'subtitle': subtitle,
            'authors': authors,
            'dates': {
                'published': article_date,
            },
            'claim': article['claimReviewed'],
            'rating': {
                'rating': article['reviewRating']['alternateName'],
                'rating_value': article['reviewRating']['ratingValue'],
                'best_rating': article['reviewRating']['bestRating'],
                'worst_rating': article['reviewRating']['worstRating'],
            },
            'body': main_text,
            'metadata': metadata,
            'tags': tags,
        }

        yield item

    @staticmethod
    def get_article_identifier(article):
        '''URL md5 hash'''
        return md5(article['url'].split('/')[-1].encode('utf8')).hexdigest()
