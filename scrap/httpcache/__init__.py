"""
HTTP Cache implementation using Redis

NOTICE: THE RESPONSE DATA WILL BE STORED IN DISK

It can be enabled by spider as:

    redis_cache_enabled = True

Request meta options

    dont_cache (if True it will be ignored)

Settings

    REDIS_CACHE_EXPIRATION_TIME: minutes
    REDIS_CACHE_DATA_PATH: the path in disk where the response data will be stored
"""

import logging

import os
import redis
from time import time
from scrapy import signals
from scrapy.utils.request import request_fingerprint
from scrapy.utils.python import to_bytes
from scrapy.responsetypes import responsetypes
from scrapy.http import Headers
from w3lib.http import headers_raw_to_dict
from w3lib.http import headers_dict_to_raw
from six.moves import cPickle as pickle


logger = logging.getLogger(__name__)


class RedisCacheMiddleware:
    '''Downloader Middleware for Scrapy'''

    def __init__(self, settings):
        self.use_cache = set()
        self.redis = self.create_redis(settings)
        self._expiration_time = settings.get('REDIS_CACHE_EXPIRATION_TIME')
        self._data_path = settings.get('REDIS_CACHE_DATA_PATH')
        if not os.path.exists(self._data_path):
            os.mkdir(self._data_path)

    @classmethod
    def from_crawler(cls, crawler):
        o = cls(crawler.settings)
        crawler.signals.connect(o.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(o.spider_closed, signal=signals.spider_closed)
        return o

    @staticmethod
    def create_redis(settings):
        '''Create a new Redis connection'''
        redis_uri = settings.get('REDIS_CACHE_URI', None)
        if redis_uri is not None:
            return redis.from_url(redis_uri)
        return redis.Redis('localhost')

    def should_use_cache(self, spider, request):
        '''Check if the spider should use HTTP Cache'''
        if spider.name not in self.use_cache:
            return False
        if request.meta.get('dont_cache', False):
            return False
        if not request.url.startswith('http'):
            return False
        return True

    def process_request(self, request, spider):
        if not self.should_use_cache(spider, request):
            return
        cached_response = self._get_cached_response(spider, request)
        if cached_response is not None:
            logger.info('RedisCacheMiddleware: using cached response for %r', request)
            return cached_response

    def process_response(self, request, response, spider):
        if not self.should_use_cache(spider, request):
            return response
        cached_response = self._get_cached_response(spider, request, is_response=True)
        if cached_response is None and response.status == 200:
            logger.info('RedisCacheMiddleware: storing response for request %r', request)
            self._store_response(spider, request, response)
        return response

    def spider_opened(self, spider):
        if getattr(spider, 'redis_cache_enabled', False):
            logger.info('RedisCacheMiddleware: cache enabled')
            self.use_cache.add(spider.name)

    def spider_closed(self, spider):
        if spider.name in self.use_cache:
            self.use_cache.remove(spider.name)

    def _read_meta(self, spider, request):
        rpath = self._get_request_path(spider, request)
        metapath = os.path.join(rpath, 'pickled_meta')
        if not os.path.exists(metapath):
            return  # not found
        with open(metapath, 'rb') as f:
            return pickle.load(f)

    def _make_response(self, spider, request):
        metadata = self._read_meta(spider, request)
        if metadata is None:
            return  # not cached

        rpath = self._get_request_path(spider, request)
        with open(os.path.join(rpath, 'response_body'), 'rb') as f:
            body = f.read()
        with open(os.path.join(rpath, 'response_headers'), 'rb') as f:
            rawheaders = f.read()
        url = metadata.get('response_url')
        status = metadata['status']
        headers = Headers(headers_raw_to_dict(rawheaders))
        respcls = responsetypes.from_args(headers=headers, url=url)
        response = respcls(url=url, headers=headers, status=status, body=body)
        return response

    def _store_response(self, spider, request, response):
        rpath = self._get_request_path(spider, request)
        if not os.path.exists(rpath):
            os.makedirs(rpath)
        metadata = {
            'url': request.url,
            'method': request.method,
            'status': response.status,
            'response_url': response.url,
            'timestamp': time(),
        }
        with open(os.path.join(rpath, 'meta'), 'wb') as f:
            f.write(to_bytes(repr(metadata)))
        with open(os.path.join(rpath, 'pickled_meta'), 'wb') as f:
            pickle.dump(metadata, f, protocol=2)
        with open(os.path.join(rpath, 'response_headers'), 'wb') as f:
            f.write(headers_dict_to_raw(response.headers))
        with open(os.path.join(rpath, 'response_body'), 'wb') as f:
            f.write(response.body)
        with open(os.path.join(rpath, 'request_headers'), 'wb') as f:
            f.write(headers_dict_to_raw(request.headers))
        with open(os.path.join(rpath, 'request_body'), 'wb') as f:
            f.write(request.body)

        self._update_request_time(request, spider)

    def _update_request_time(self, request, spider):
        key = self._get_request_hash(request, spider)
        self.redis.set(key, '{}'.format(time()))

    def _get_request_hash(self, request, spider):
        request_key = None
        if hasattr(spider, 'redis_cache_make_request_key'):
            if callable(spider.redis_cache_make_request_key):
                request_key = spider.redis_cache_make_request_key(request)
        if request_key is None:
            request_key = request_fingerprint(request)
        return request_key

    def _get_cached_response(self, spider, request, *, is_response=False):
        request_key = self._get_request_hash(request, spider)
        cache_data = self.redis.get(request_key)
        if cache_data is None:
            return None
        cached_time = cache_data.decode('utf-8')
        if 0 < self._expiration_time < time() - float(cached_time):
            if not is_response:
                logger.info('RedisCacheMiddleware: Expired cache for %r', request)
            return None
        return self._make_response(spider, request)

    def _get_request_path(self, spider, request):
        key = self._get_request_hash(request, spider)
        return os.path.join(self._data_path, spider.name, key[0:2], key)
