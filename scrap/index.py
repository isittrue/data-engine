"""
Given a spider key/name it will return the spider class
"""

import os
import inspect
import importlib

from scrapy import Spider


HERE = os.path.abspath(os.path.dirname(__file__))

SPIDERS_DIR = os.path.join(HERE, 'spiders')


def add_spiders(project_path, spiders: dict):
    '''Given a project path it will add the spiders to the `spiders` dict
    <spider_name> => <spider_class>'''
    py_files = [
        os.path.join(project_path, py_fname) for py_fname in os.listdir(project_path)
        if py_fname != '__init__.py' and py_fname.endswith('.py')]
    pmodname = project_path.split('/')[-1]
    for py_file_path in py_files:
        smodname = py_file_path.split('/')[-1][:-3]  # Removed .py at the end
        try:
            mod = importlib.import_module('scrap.{}.{}'.format(pmodname, smodname))
            for _, mod_elem in  inspect.getmembers(mod):
                if not inspect.isclass(mod_elem):
                    continue
                if issubclass(mod_elem, Spider):
                    if not getattr(mod_elem, 'name'):
                        continue
                    spiders[mod_elem.name] = mod_elem
        except ImportError:
            continue
    return spiders


class SpiderIndex:
    '''The Project and Spider Index'''

    def __init__(self):
        self.spiders = {}
        self.load_spiders()

    def load_spiders(self):
        '''Read the project directories and add the spiders found there'''
        add_spiders(SPIDERS_DIR, self.spiders)

    def __getitem__(self, sname):
        return self.spiders.get(sname)

    def keys(self):
        '''Return the name of the spiders in the index'''
        return self.spiders.keys()
