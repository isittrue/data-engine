"""
Downloader Middleware to catch and store connection Exceptions

IMPORTANT: Depends on CrawlerExtension
"""

from datetime import datetime

import pymongo

from scrapy import signals


class ErrorsDownloaderMiddleware:
    '''Process Exceptions and store them in DB'''

    collection_name = 'errors'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        '''Create a new middleware from settings'''
        mdlw = cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'isittrue')
        )
        crawler.signals.connect(mdlw.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(mdlw.spider_closed, signal=signals.spider_closed)
        return mdlw

    def spider_opened(self, spider):
        '''When the spider is opened it create a new MongoDB conection'''
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def spider_closed(self, spider):
        '''Close the mongo client'''
        self.client.close()

    def process_exception(self, request, exception, spider):
        '''Process an exception in the downloader'''
        self.db[self.collection_name].insert_one({
            'spider': spider.name,
            'crawl_uuid': spider.crawl_uuid,
            'time_created': datetime.now(),
            'type': 'Downloader Exception',
            'value': f'{exception}',
            'request': f'{request}'})
