"""
Look for articles in the DB

It returns the data if the article exists.

You sould send the search as metadata in the request using the format

    cache__<field>: <value>

For example:

    response.meta['cache__url'] = 'https://www.scopes.com/fact-check/article-path'
"""

import logging

import pymongo

from scrapy import Request
from scrapy import signals


logger = logging.getLogger(__name__)


DUMMY_URL = 'file:///etc/hosts'


class ArticleCacheDownloaderMiddleware:
    '''Downloader Middleware'''

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

        # to be initialized when the spider is opened
        self.client = None
        self.db = None
        self.collection_name = ''

    def from_cache(self, response):
        logger.info('Cache found for items in %s', response.meta['url'])
        for item in response.meta['data']:
            item['__cached'] = True  # MongoDB pipeline will ignore this item
            yield item

    @classmethod
    def from_crawler(cls, crawler):
        article_cache_dm = cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'isittrue')
        )
        crawler.signals.connect(article_cache_dm.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(article_cache_dm.spider_closed, signal=signals.spider_closed)
        return article_cache_dm

    def spider_opened(self, spider):
        '''Init a new MongoDB client and get/create the database'''
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.collection_name = spider.name

    def spider_closed(self, spider):
        '''Close the mongo client'''
        self.client.close()

    def process_request(self, request, spider):
        '''If `cache__key` is passed in `request.meta` then it will try to locate
        the data in DB and return the result if the data exists'''
        search = self.get_cache_search_from_request(request)
        if search:
            data = self.get_data_from_db(search)
            if data:
                return Request(DUMMY_URL,
                               meta={'data': data,
                                     'url': request.url,
                                     'pp_enabled': False,
                                     'ps_disabled': True,
                                     'keep_session': True,
                                     'dont_cache': True},
                               callback=self.from_cache,
                               dont_filter=True)

    def get_data_from_db(self, search):
        results = []
        for result in self.db[self.collection_name].find(search):
            results.append(result)
        return results

    @staticmethod
    def get_cache_search_from_request(request):
        search = {}
        for m_key in request.meta:
            if m_key.startswith('cache__'):
                search[m_key.split('cache__', 1)[-1]] = request.meta[m_key]
        return search
