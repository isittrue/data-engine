"""
Scrapy Downloader Middlewares
"""


class LogProxy:
    '''Middleware to log the current proxy in use'''
    def process_request(self, request, spider):
        if 'proxy' in request.meta:
            spider.logger.info('Request %r using proxy %s', request, request.meta['proxy'])


class CustomHeadersMiddleware:
    '''Middlewares for using custom headers for spiders'''

    def process_request(self, request, spider):
        if hasattr(spider, 'custom_headers'):
            request.headers.update(spider.custom_headers)
