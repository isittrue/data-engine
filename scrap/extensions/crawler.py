"""
CrawlerExtension

Extension to set and store crawler attributes and stats
"""

import uuid

from datetime import datetime

import pymongo

from scrapy import signals


class CrawlerExtension:

    collection = 'crawlers'

    def __init__(self, mongo_uri: str, mongo_db: str):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        '''Create a new extension from settings'''
        extension = cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'isittrue'),
        )
        crawler.signals.connect(extension.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(extension.spider_closed, signal=signals.spider_closed)
        return extension

    def spider_opened(self, spider):
        spider.crawl_uuid = str(uuid.uuid4())

        with pymongo.MongoClient(self.mongo_uri) as mongo_client:
            db = mongo_client[self.mongo_db]
            db[self.collection].insert_one({
                'spider': spider.name,
                'crawl_uuid': spider.crawl_uuid,
                'started_time': datetime.now()})

    def spider_closed(self, spider):
        with pymongo.MongoClient(self.mongo_uri) as mongo_client:
            db = mongo_client[self.mongo_db]
            db[self.collection].update_one(
                {'crawl_uuid': spider.crawl_uuid},
                {'$set': {'finished_time': datetime.now(),
                          'stats': spider.crawler.stats.get_stats()}}, upsert=True)
