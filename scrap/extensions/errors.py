"""
Extension to read stored Exceptions/errors and do something with that

- Send email

IMPORTANT: Depends on CrawlerExtension
"""

import pymongo

from scrapy import signals
from scrapy.mail import MailSender

from config import get_config


class ErrorsExtension:
    '''Errors management Scrapy extension'''

    collection = 'errors'

    def __init__(self, mongo_uri: str, mongo_db: str, mailer: MailSender):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.mailer = mailer

    @classmethod
    def from_crawler(cls, crawler):
        '''Create a new extension from settings'''
        extension = cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'isittrue'),
            mailer=MailSender.from_settings(crawler.settings)
        )
        crawler.signals.connect(extension.spider_closed, signal=signals.spider_closed)
        return extension

    def spider_closed(self, spider):
        '''When the spider is closed it reads the DB and collect all the errors.
        If some errors found it will send an email to notify that errors found for this spider'''
        receivers = self.get_receivers()
        if receivers is not None:
            with pymongo.MongoClient(self.mongo_uri) as mongo_client:
                collection = mongo_client[self.mongo_db][self.collection]
                errors_count = self._get_errors_count(spider, collection)
                if errors_count > 0:
                    subject = f'Spider - Errors found for "{spider.name}"'
                    message = f'{errors_count} errors found'
                    for receiver in receivers:
                        self.mailer.send(to=[receiver], subject=subject, body=message)

    @staticmethod
    def _get_errors_count(spider, collection):
        return collection.count_documents({
            'spider': spider.name,
            'crawl_uuid': spider.crawl_uuid,
        })

    @staticmethod
    def get_receivers() -> list:
        '''Read the config and return the emails'''
        config = get_config()
        if 'notifications' not in config:
            return None
        if 'errors' not in config['notifications']:
            return None
        return config['notifications']['errors']
