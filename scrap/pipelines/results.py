"""
Results pipeline

Uses the `results` instance if present and adds items to it
"""

import logging

from itemadapter import ItemAdapter

logger = logging.getLogger(__name__)


class ResultsPipeline:
    '''Results pipeline'''

    def process_item(self, item, spider):
        '''Add items to the `results` instance'''
        results = getattr(spider, 'results', None)
        if results is not None:
            logger.info('Add item to results %r', item)
            results.append(ItemAdapter(item).asdict())
        return item
