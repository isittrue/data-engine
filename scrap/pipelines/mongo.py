"""
Export Items to MongoDB
"""

import logging

from datetime import datetime

import pymongo

from itemadapter import ItemAdapter


logger = logging.getLogger(__name__)


class ExportMongoPipeline:
    '''Main class to export items to MongoDB'''

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

        # to be initialized when the spider is opened
        self.client = None
        self.db = None
        self.collection_name = ''

    @classmethod
    def from_crawler(cls, crawler):
        '''Create a new pipeline from settings'''
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'isittrue')
        )

    def open_spider(self, spider):
        '''Init a new MongoDB client and get/create the database'''
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.collection_name = spider.name

    def close_spider(self, spider):
        '''Close the MongoDB client'''
        self.client.close()

    def process_item(self, item, spider):
        '''Create and add/update the document in database'''
        if not item.get('__cached', False):
            document = self.create_document_from_item(item, spider)
            if 'id' in item:
                item_id = document.pop('id')
                # Upsert
                self.db[self.collection_name].update_one(
                    {'id': item_id}, {'$set': document}, upsert=True)
            else:
                # Only insert
                self.db[self.collection_name].insert_one(document)
        return item

    @staticmethod
    def create_document_from_item(item, spider):
        '''It will create a new document to be added to the results'''
        document = {
            'time_created': datetime.now(),
        }
        document.update(ItemAdapter(item).asdict())
        return document
