"""
Firefox Headless
"""

import logging
import time
from selenium.webdriver import Firefox
from selenium.webdriver import FirefoxProfile
from selenium.webdriver.firefox.options import Options


logger = logging.getLogger(__file__)


class FirefoxHeadless:
    '''Firefox Headless controler class'''

    BINARY_PATH = '/usr/bin/firefox'
    DRIVER_PATH = '/usr/bin/geckodriver'
    DEFAULT_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
    DOWNLOAD_MIME = (
        'text/plain',
        'application/vnd.ms-excel',
        'text/csv',
        'text/comma-separated-values',
        'application/octet-stream',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/pdf',
        'application/zip',
        'application/x-zip-compressed',
        'multipart/x-zip')

    def __init__(self, *args, create=True, **kwargs):
        self._driver = None

        if create:
            self.init_browser(*args, **kwargs)

    def init_browser(self, *args, **kwargs):
        '''A wrapper for create_driver'''
        created = False
        retry = 5
        error = None
        while not created and retry:
            try:
                self._driver = FirefoxHeadless.create_driver(*args, **kwargs)
            except Exception as e:
                retry -= 1
                error = e
            else:
                created = True

        # If error found
        if not created and error:
            raise error
        elif not created:
            raise Exception('Could not instantiate Firefox, an error has occurred')

    def get(self, url, sleep=5, retry=4):
        ok = False
        error = None
        while not ok and retry:
            try:
                self._driver.get(url)
                time.sleep(sleep)
            except Exception as e:
                retry -= 1
                error = e
            else:
                ok = True
        if not ok and error:
            raise error
        elif not ok:
            raise Exception('Could not get {}, an error has occurred'.format(url))

    def close(self):
        self._driver.quit()

    @property
    def driver(self):
        return self._driver

    @classmethod
    def from_settings(cls, settings, **kwargs):
        '''Create a new instance from the spider/crawler settings'''
        kwargs.update(cls.get_kwargs_from_settings(settings))
        return cls(**kwargs)

    @classmethod
    def get_kwargs_from_settings(cls, settings):
        kwargs = {
            'user_agent': settings.get('USER_AGENT', cls.DEFAULT_AGENT),
            'binary_path': settings.get('FIREFOX_BINARY', cls.BINARY_PATH),
            'driver_path': settings.get('FIREFOX_DRIVER', cls.DRIVER_PATH),
        }
        return kwargs

    @classmethod
    def create_driver(cls, binary_path=None, driver_path=None, user_agent=None, proxy=None,
                      timeout=180,
                      download_dir='/tmp'):
        '''Create the driver and return it'''
        options = Options()
        options.add_argument('-headless')

        profile = FirefoxProfile()

        if user_agent is not None:
            profile.set_preference('general.useragent.override', user_agent)
        else:
            profile.set_preference('general.useragent.override', cls.DEFAULT_AGENT)

        profile.set_preference('browser.download.folderList', 2) # custom location
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', download_dir)
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk',
                               ', '.join(cls.DOWNLOAD_MIME))

        if proxy is not None:
            proxy_host, proxy_port = proxy.split(':')
            profile.set_preference('network.proxy.type', 1)
            profile.set_preference('network.proxy.http', proxy_host)
            profile.set_preference('network.proxy.http_port', int(proxy_port))
            profile.set_preference('network.proxy.ssl', proxy_host)
            profile.set_preference('network.proxy.ssl_port', int(proxy_port))
            profile.set_preference('network.proxy.ftp', proxy_host)
            profile.set_preference('network.proxy.ftp_port', int(proxy_port))
            profile.set_preference('network.proxy.socks', proxy_host)
            profile.set_preference('network.proxy.socks_port', int(proxy_port))
            profile.set_preference('network.proxy.share_proxy_settings', True)
            profile.set_preference('network.proxy.backup.ssl', '')
            profile.set_preference('network.proxy.backup.ssl_port', 0)
            profile.set_preference('network.proxy.backup.socks', '')
            profile.set_preference('network.proxy.backup.socks_port', 0)
            profile.set_preference('network.proxy.backup.ftp', '')
            profile.set_preference('network.proxy.backup.ftp_port', 0)
            profile.set_preference('network.proxy.autoconfig_url', '')
            profile.set_preference('network.proxy.autoconfig_url.include_path', False)
            profile.set_preference('network.proxy.autoconfig_retry_interval_max', 300)
            profile.set_preference('network.proxy.autoconfig_retry_interval_min', 5)
            profile.set_preference('network.proxy.socks_version', 5)
            profile.set_preference('network.proxy.no_proxies_on', 'localhost, 127.0.0.1')
            profile.set_preference('network.proxy.failover_timeout', 1800)
            profile.set_preference('network.proxy.proxy_over_tls', True)
            profile.set_preference('network.proxy.socks_remote_dns', False)
            profile.set_preference('media.peerconnection.identity.enabled', False)
            profile.set_preference('media.peerconnection.identity.timeout', 1)
            profile.set_preference('media.peerconnection.turn.disable', True)
            profile.set_preference('media.peerconnection.use_document_iceservers', False)
            profile.set_preference('media.peerconnection.video.enabled', False)
            profile.set_preference('privacy.donottrackheader.enabled', True)
            profile.set_preference('privacy.trackingprotection.enabled', True)

        profile.update_preferences()

        if binary_path is None:
            binary_path = cls.BINARY_PATH

        if driver_path is None:
            driver_path = cls.DRIVER_PATH

        driver = Firefox(profile,
                         firefox_binary=binary_path,
                         executable_path=driver_path,
                         firefox_options=options,
        )

        driver.set_window_size(1920, 1080)

        driver.set_page_load_timeout(timeout)
        driver.set_script_timeout(timeout)

        return driver

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            logger.error('FirefoxHeadless error: %r, %r', exc_type, exc_val)
            logger.error('FirefoxHeadless error trace: %r', exc_tb)
        if self._driver:
            self.close()
