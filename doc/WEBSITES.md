## Websites

**US**

- [x] [afp.com](https://factcheck.afp.com/afp-usa)

- [x] [factcheck.org](https://FactCheck.org)

- [x] [snopes.com](https://snopes.com)

- [ ] [leadstories.com](https://leadstories.com)

- [ ] [checkyourfact.com](https://checkyourfact.com)

- [ ] [washingtonpost.com](https://www.washingtonpost.com/news/fact-checker/) (ON HOLD)

- [ ] ~~[mediabiasfactcheck.com](https://mediabiasfactcheck.com/)~~ (NOT A FACTCHECKER ITSELF)

- [ ] [our.news](https://our.news/')

- [ ] [politiFact](https://www.politifact.com/')

- [ ] [realClearPolitics](https://www.realclearpolitics.com/)

**CA**

- [ ] [afp](https://factcheck.afp.com/afp-canada)

- [ ] [FactsCan](http://factscan.ca/)

**UK**

- [ ] [BBCRealityCheck](https://www.bbc.com/news/reality_check)

- [ ] [fullFact](https://fullfact.org/)

- [ ] [factCheckNI](https://factcheckni.org/)

- [ ] [Ferret](https://theferret.scot/ferret-fact-service/)

**AU**

- [ ] [ABCFactCheck](https://www.abc.net.au/news/factcheck/)
