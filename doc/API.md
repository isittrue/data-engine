## API

**GET /search?api_key=<...>**

- `site` (*): website/spider name

- `query` (*): the website search

- `pages`: number of pages to crawl, default: `5`
