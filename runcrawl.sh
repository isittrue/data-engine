#!/bin/sh

SITE=$1
NOW=$(date +"%Y-%m-%d_%T")

export PYTHONPATH=$PYTHONPATH:.

python scrap/run.py --spider $SITE --directory "data/spiders/${SITE}_${NOW}"
